﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP
{
    class AccountA : IBankAccount
    {
        private decimal balance;
        public decimal Balance { get { return balance; } }

        public void Deposit(decimal amount)
        {
            balance += amount;
        }

        public bool TransferTo(IBankAccount account, decimal amount)
        {
            bool withdrawn = this.Withdraw(amount);
            if (withdrawn)
                account.Deposit(amount);
            return false;
        }

        public bool Withdraw(decimal amount)
        {
            if (balance > amount)
            {
                balance -= amount;
                return true;
            }
            Console.WriteLine("Za malo funduszy na koncie zeby wyplacic");
            return false;
        }
    }
}
