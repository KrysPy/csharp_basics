﻿using System.Collections;
using System.Collections.Generic;

namespace OOP
{
    class Program
    {
        static void Display(Point p)
        {
            //if (p is Point3D)
            //    Console.WriteLine(((Point3D)p).getCords());
            //else
            //    Console.WriteLine(p.getCords());
            
            Point3D p3d = p as Point3D; // kiedy nie jest możliwe to p3d bedzie null
            if (p3d == null)
                Console.WriteLine(p.getCords());
            else
                Console.WriteLine(p3d.getCords());

        }

        // ENUMERATION
        public enum TimeOfDay
        {
            Morning = 0, Afternoon = 1, Evening = 2
        }

        public static string getGreetings(TimeOfDay time)
        {
            switch (time)
            {
                case TimeOfDay.Morning:
                    return "Good morning";
                case TimeOfDay.Afternoon:
                    return "Good Afternoo";
                case TimeOfDay.Evening:
                    return "Good evening";
                default:
                    return "Sth went wrowng";
            }
        }

        public static void Swap<T> (ref T a, ref T b)
        {
            T tmp;
            tmp = a;
            a = b;
            b = tmp;
        }

        public static int? getSomethingFromDataBase()
        {
            return null; 
        }

        static void Main(string[] args)
        {
            Gamer[] gamers = new Gamer[3];
            gamers[0] = new Gamer("Gamer 1");
            gamers[1] = new Gamer("Gamer 2");
            gamers[2] = new Gamer("Gamer 3");

            Console.WriteLine(gamers[0].Nickname);
            Console.WriteLine(gamers[1].Nickname);
            Console.WriteLine(gamers[2].Nickname);


            var customer = new Customer();
            customer.Id = 1;
            customer.Name = "John";
            
            var order = new Order();
            customer.Orders.Add(order);

            Console.WriteLine(customer.Id);
            Console.WriteLine(customer.Name);

            Console.WriteLine("Absolute: " + Mathematica.Abs(0));
            int a = 10;
            Mathematica.IncreaseBy5(a);
            Console.WriteLine("VAluea : " + a);

            int b;
            Mathematica.Zero(out b);

            Console.WriteLine(Welcomer.Message);

            Console.WriteLine("-----------------------------------");

            Point point = new Point(5, 10);
            Console.WriteLine(point.X);
            Console.WriteLine(point.Y);

            Point3D point3D = new Point3D(6, 7, 8);
            Console.WriteLine(point3D.X);
            Console.WriteLine(point3D.Y);
            Console.WriteLine(point.getCords());

            Console.WriteLine("-----------------------------------");
            Point c;
            Point d;

            c = new Point(1, 5);
            d = new Point3D(1, 5, 10);

            Display(c);
            Display(d);


            Console.WriteLine("-----------------------------------");
            var shapes = new List<Shape>()
            {
                new Square(),
                new Triangle()
            };

            Console.WriteLine("-----------------------------------");

            Person[] people = new Person[4];
            people[0] = new Employee("John", "Smith", 300000); 
            people[1] = new Employee("Arc", "Smith", 100000); 
            people[3] = new Student("K", "B", 4);

            Person.DisplayPeople(people);
            
            Console.WriteLine("-----------------------------------");

            IBankAccount firstAccount = new AccountA();
            IBankAccount secondAccount = new AccountA();
            firstAccount.Deposit(500);
            secondAccount.Deposit(500);

            firstAccount.TransferTo(secondAccount, 700);
            Console.WriteLine($"Konto pierwsze: {firstAccount.Balance}");
            Console.WriteLine($"Konto drugie: {secondAccount.Balance}");

            Console.WriteLine("-----------------------------------");
            // enum
            TimeOfDay t = TimeOfDay.Morning;
            TimeOfDay t2 = (TimeOfDay)1;
            Console.WriteLine(getGreetings(t));
            Console.WriteLine(getGreetings(t2));

            Console.WriteLine("-----------------------------------");
            ArrayList list = new ArrayList();
            list.Add(5);
            list.Add(10);
            list.Add(15);
            list.Add(20);
            list.Add("asdeff");

            List<Test> testsList = new List<Test>();
            testsList.Add(new Test("Wartosc z egzemplarza klasy Test"));
            testsList.Add(new Test("Elo"));

            foreach (var element in testsList)
            {
                Console.WriteLine(element.TestowyString);
            }
            List<int> integersList = new List<int>();
            integersList.Add(10);
            integersList.Add(30);
            integersList.Add(11);

            var zm = new Simple<String>();

            Console.WriteLine("-----------------------------------");

            AccountBalance ab1 = new AccountBalance(5, 120);
            AccountBalance ab2 = new AccountBalance(5, 10);

            Console.WriteLine(ab1 == ab2);
            Console.WriteLine(ab1 + ab2);
            Console.WriteLine(ab1.ToString());

            Console.WriteLine("-----------------------------------");

            int? x1 = getSomethingFromDataBase();
    
            if (x1 == null)
                Console.WriteLine("Nie podales a");
            else
                Console.WriteLine(x1.ToString());

            Console.WriteLine("-----------------------------------");
            int c1 = 20;
            int c2 = 0;
            try
            {
                Console.WriteLine(a / b);
            }
            catch (DivideByZeroException e)
            {
                Console.WriteLine(e.Message);
            }
            finally
            {
                Console.WriteLine("Pokaze sie zawsze, sluzy do czyszcenia, zamykania plikow, polaczen");
            }


        }
    }
}