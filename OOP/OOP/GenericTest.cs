﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP
{
    class GenericTest<T> where T : class
    {
        public T Value { get; private set; }
        public GenericTest()
        {
            this.Value = default(T);
        }

    }   
}
