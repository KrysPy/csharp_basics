﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP
{
    class DoubleCalculator<U> : Calculator<double>
    {
        public override double Add(double x, double y)
        {
            return x + y;
        }

        public override double Substract(double x, double y)
        {
            return x - y;
        }
    }
}
