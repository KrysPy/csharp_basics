﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP
{
    class Employee : Person
    {
        public double Salary { get; protected set; }
        
        public Employee() : base()
        {
            this.Salary = 0;
        }
        
        public Employee(string name, string surname, double salary)
            : base(name, surname)
        {
            this.Salary = salary;
        }
        override public string Description()
        {
            return $"{this.Name} {this.Surname}, wynagrodzenie to: {this.Salary}";
        }
    }
}
