﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP
{
    class Point
    {
        public int X { get; protected set; }
        public int Y { get; protected set; }

        public Point() 
        {
            X = 0;
            Y = 0;
        }

        public Point(int x, int y)
        {
            this.X = x;
            this.Y = y;
        }

        public virtual string getCords()
        {
            return $"{this.X}, {this.Y}";
        }

    }
}
