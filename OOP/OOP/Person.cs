﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP
{
    abstract class Person
    {
        public string Name { get; protected set; }
        public string Surname { get; protected set; }

        public Person() 
        {
            this.Name = "";
            this.Surname = "";
        }
        
        public Person(string name, string surname)
        {
            this.Name = name;
            this.Surname = surname;
        }
        abstract public string Description();

        public static void DisplayPeople(Person[] people)
        {
            foreach (Person p in people)
            {
                if (p != null)
                    Console.WriteLine(p.Description());
            }
        }
    }
}
