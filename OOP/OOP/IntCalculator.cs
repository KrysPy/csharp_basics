﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP
{
    class IntCalculator : Calculator<int>
    {
        public override int Add(int x, int y)
        {
            return x + y;
        }

        public override int Substract(int x, int y)
        {
            return x - y;
        }
    }
}
