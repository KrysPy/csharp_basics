﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP
{
    class Point3D : Point
    {
        public int Z { get; protected set; }

        public Point3D(int x, int y, int z) : base(x, y)
        {
            this.Z = z;   
        }
        public override string getCords()
        {
            return base.getCords() + ", " + this.Z;
        }
    }
}
