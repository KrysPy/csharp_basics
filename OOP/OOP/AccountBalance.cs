﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP
{
    class AccountBalance
    {
        public uint Zl { get; private set; }
        private ushort gr;
        public ushort Gr
        {
            get { return this.gr; }
            private set
            {
                if (value <= 0)
                    this.gr = 0;
                else if (value >= 100)
                {
                    uint ZlotysInGros = (uint)(value / 100);
                    Zl += ZlotysInGros;
                    this.gr = (ushort)(value - ZlotysInGros * 100);
                }
                else
                    this.gr = value;
            }
        }

        public AccountBalance(uint zl, ushort gr)
        {
            this.Zl = zl;
            this.Gr = gr;   
        }
        public AccountBalance(AccountBalance ab)
        {
            this.Gr = ab.gr;
            this.Zl = ab.Zl;
        }

        public static bool operator ==(AccountBalance ab1, AccountBalance ab2)
        {
            if (ab1.Zl == ab2.Zl && ab1.Gr == ab2.Gr)
                return true;
            return false;
        }

        public static bool operator !=(AccountBalance ab1, AccountBalance ab2)
        {
            return !(ab1 == ab2);
        }
        public static AccountBalance operator +(AccountBalance ab1, AccountBalance ab2)
        {
            uint tmpZl = ab1.Zl + ab2.Zl;
            ushort tmpGr = (ushort)(ab1.Gr + ab2.Gr);

            uint ZlotysInGros = (uint)(tmpGr / 100);
            tmpZl += ZlotysInGros;
            tmpGr = (ushort)(tmpZl - ZlotysInGros * 100);

            return new AccountBalance(tmpZl, tmpGr);
        }

        public override string ToString()
        {
            return Zl + "zł i " + Gr + "gr";   
        }

        public static implicit operator float(AccountBalance value)
        {
            return value.Zl + (value.Gr / 100.0f);
        }
        public static explicit operator AccountBalance(float value)
        { 
            ushort tmpGr = (ushort)((value - (uint)value) * 100);
            return new AccountBalance((uint)value, tmpGr); 
        }
    }
}
