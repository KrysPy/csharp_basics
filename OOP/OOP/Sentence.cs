﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP
{
    class Sentence
    {
        public string[] words;

        public Sentence(string sentence)
        {
            this.words = sentence.Split();
        }

        public string this[int n]
        {
            get 
            {
                if (words.Length > n)
                    return words[n];
                else
                    return "Error";
            }

            set 
            {
                if (words.Length > n)
                    words[n] = value; 
            }
        }
    }
}
