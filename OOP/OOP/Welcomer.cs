﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP
{
    class Welcomer
    {
        public static string Message
        {
            get;
            private set;
        }

        static Welcomer()
        {
            DateTime Now = DateTime.Now;
            if (Now.Hour < 12)
            {
                Message = "Good Morning";
            }
            else if (12 < Now.Hour && Now.Hour < 19)
            {
                Message = "Good afternoon";
            } 
            else
            {
                Message = "Good evening";
            }

        }
    }
}
