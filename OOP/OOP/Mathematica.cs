﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP
{
    class Mathematica
    {
        public static int Add(params int[] args)
        {
            int sum = 0;
            foreach (int arg in args)
            {
                sum += arg;
            }
            return sum;
        }
        
        public static int Amplify(int powerBase, int exponent = 2)
        {
            int tmp = 1;

            for (int i = 0; i < exponent; i++)
            {
                tmp *= powerBase;
            }

            return tmp;
        }

        public static readonly double PI = 3.14;
    
        public static int Abs(int x)
        {
            return (x < 0) ? -x : x;
        }

        public static int IncreaseBy5(ref int val)
        {
            return val += 5;
        }

        public static int IncreaseBy5(int val)
        {
            return val += 5;
        }

        public static void Zero(out int b)
        {
            b = 0;
        }
    }
}
