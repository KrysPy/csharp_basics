﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP
{
    abstract class Calculator<T> where T : struct
    {
        public abstract T Add(T x, T y);
        public abstract T Substract(T x, T y);
    }
}
