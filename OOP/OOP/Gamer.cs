﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP
{
    class Gamer
    {
        string nickname;
        int id;
        static int nextId = 0;

        public Gamer(string nickname)
        {
            this.nickname = nickname;
            nextId++;
            id = nextId;
        }

        public string Nickname
        {
            get { return nickname; }
         
            set
            {
                // here we can add some conditions 
                this.nickname = value;
            }
        }

        public int Id
        {
            get { return id; }
        }
    }
}
