﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP
{
    class Simple <Whatever>
    {
        Whatever variableName;

        public Whatever getValue()
        {
            return this.variableName;
        }

        public void setValue(Whatever variableName)
        {
            this.variableName = variableName;
        }
    }
}
