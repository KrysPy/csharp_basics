﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP
{
    static class StringHelper
    {
        public static bool IsCapitalized(this String value)
        {
            return Char.IsUpper(value[0]);
        }
    }
}
