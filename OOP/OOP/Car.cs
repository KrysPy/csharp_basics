﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP
{
    class Car
    {
        public string Description { get; set; }
        public byte NrOfWheels { get; private set; }

        private Engine engine;

        public Car(string description, byte nrOfWheels = 4)
        {
            Description = description;
            NrOfWheels = nrOfWheels;
            engine = new Engine(100);
        }
        // konstruktor wywolujacy inny konstruktor
        public Car(string description) : this(description, 4)
        {
             
        }
        // konstruktor kopiujacy 
        public Car(Car car)
        {
            this.Description = car.Description;
            this.NrOfWheels = car.NrOfWheels;
        }

        private class Engine
        {
            public uint Power;

            public Engine(uint power = 100)
            {
                this.Power = power;
            }
        }

    }
}
