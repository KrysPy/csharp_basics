﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP
{
    class Student : Person
    {
        public byte Semester { get; protected set; }
        
        public Student() : base()
        {
            this.Semester = 1;
        }
        public Student(string name, string surname, byte semester)
            : base(name, surname)
        {
            this.Semester = semester;
        }
        override public string Description()
        {
            return $"{this.Name} {this.Surname}, semestr to: {this.Semester}";
        }
    }
}
