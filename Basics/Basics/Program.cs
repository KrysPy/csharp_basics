﻿namespace Basics
{
    class Program
    {
        static void Main(string[] args)
        {

            // multidimensional arrays
            string[,] tab2D = new string[4,3];
            Console.WriteLine("Mulitdimensional Array: ");
            for (int i = 0; i < tab2D.GetLength(0); i++)
            {
                for(int j = 0; j < tab2D.GetLength(1); j++)
                {
                    tab2D[i, j] = "random";
                }
            }
            for (int i = 0; i < tab2D.GetLength(0); i++)
            {
                for (int j = 0; j < tab2D.GetLength(1); j++)
                {
                   Console.Write(tab2D[i, j] + " ");
                }
                Console.WriteLine();
            }

            // jagged arrays
            Console.WriteLine("Jagged Arrays");
            int[][] jaggedArray = new int[3][];
            jaggedArray[0] = new int[] { 4, 6, 1 };
            jaggedArray[1] = new int[] { 5, 4 };
            jaggedArray[2] = new int[] { 4, 2, 1, 4, 5 };

            for (int i = 0;i < jaggedArray.GetLength(0); i++)
            {
                for (int j = 0; j < jaggedArray[i].Length; j++)
                {
                    Console.Write(jaggedArray[i][j] + " ");
                }
                Console.WriteLine();
            }
        }
    }
}